<?php get_header(); ?>

        <div class="container">

            <ul class="breadcrumb">
                <li> <a href="<?php echo get_option( "home" ); ?>">Home</a> </li>
                <li class="active"> <?php the_title(); ?> </li>
            </ul>

            <!-- Example row of columns -->
            <div class="row">
                <div class="col-xs-12">
                    <h2><?php the_title(); ?></h2>
                    <hr>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-9"> 
                    <div class="row"> <?php 
                        $args = array(
                        'depth'        => 0,
                        'show_date'    => '',
                        'date_format'  => get_option('date_format'),
                        'child_of'     => 0,
                        'exclude'      => '',
                        'include'      => '',
                        'title_li'     => __('Pages'),
                        'echo'         => 1,
                        'authors'      => '',
                        'sort_column'  => 'menu_order, post_title',
                        'link_before'  => '',
                        'link_after'   => '',
                        'walker'       => '',
                        'post_type'    => 'page',
                        'post_status'  => 'publish' 
                    ); ?>
                        <div class="col-md-4"><?php wp_list_pages( $args ); ?></div>


                        <?php $args = array(
                            'show_option_all'    => '',
                            'orderby'            => 'name',
                            'order'              => 'ASC',
                            'style'              => 'list',
                            'show_count'         => 0,
                            'hide_empty'         => 1,
                            'use_desc_for_title' => 1,
                            'child_of'           => 0,
                            'feed'               => '',
                            'feed_type'          => '',
                            'feed_image'         => '',
                            'exclude'            => '',
                            'exclude_tree'       => '',
                            'include'            => '',
                            'hierarchical'       => 1,
                            'title_li'           => __( 'Categorias das Notícias' ),
                            'show_option_none'   => __('No categories'),
                            'number'             => null,
                            'echo'               => 1,
                            'depth'              => 0,
                            'current_category'   => 0,
                            'pad_counts'         => 0,
                            'taxonomy'           => 'category',
                            'walker'             => null
                        ); ?> 

                        <div class="col-md-4"> <?php 
                            // wp_list_categories( $args );  ?>

                            <li>Categoria dos Artigos 
                                <ul> <?php 
                                $categorias = get_terms( "tx-artigo" );
                                foreach($categorias as $categoria) { ?>    
                                    <li><a href="<?php echo get_term_link( $categoria, "tx-artigo" ); ?>"><?php echo $categoria->name; ?></a></li><?php                            
                                } ?> 
                                </ul>
                            </li>

                            <li>Categoria das Novidades 
                                <ul> <?php 
                                $categorias = get_terms( "tx-novidade" );
                                foreach($categorias as $categoria) { ?>    
                                    <li><a href="<?php echo get_term_link( $categoria, "tx-novidade" ); ?>"><?php echo $categoria->name; ?></a></li><?php                            
                                } ?> 
                                </ul>
                            </li>
                        </div>


                        <div class="col-md-4">Usuarios <br><?php wp_list_authors(  ); ?></div>
                    </div>
                
                </div> <!-- .col-md-9 -->
                
                <aside class="col-md-3">

                    <?php if ( !dynamic_sidebar( 'barra-lateral-widget' )) {  } ?>
                    
                    <hr>

                    
                </aside>
            </div>
        </div>
        <!-- /container -->



<?php get_footer(); ?>