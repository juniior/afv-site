<?php get_header(); ?>

        <div class="container">

            <ul class="breadcrumb">
                <li>
                    <a href="<?php echo get_option( "home" ); ?>">Home</a>
                </li>
                <li class="active"><?php the_title(); ?>
                </li>
            </ul>

            <!-- Example row of columns -->
            <div class="row">
                <div class="col-xs-12">
                    <h2><?php the_title(); ?></h2>
                    <hr>
                </div>
            </div>
            
            <div class="row"><?php 
                $parametros = array("post_type" => "cpt_novidade", 'orderby' => 'date', 'order' => 'desc', 'posts_per_page' => 20, 'paged' => ((get_query_var('paged')) ? get_query_var('paged') : 1), 'cat' => get_query_var('cat') );   
                $agenda = new WP_Query($parametros); ?>
                <div class="col-md-9"> <?php  
                if( $agenda->have_posts()) { 
                    while($agenda->have_posts()) {
                        $agenda->the_post(); ?>



                         <div class="item">
                                <div class="row">
                                    <div class="col-md-1">
                                        <div class="item-data-dia"><?php the_time('j') ?></div>
                                        <div class="item-data-mes"><?php the_time('M') ?></div>
                                    </div>
                                    <div class="col-md-11">
                                         <div class="media-body">
                                            <h4 class="media-heading"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                            <p><small>[ <?php wp_title(""); ?> ] Postado por <?php the_author_posts_link(); ?> em <?php the_time('j \d\e F \d\e Y') ?></small></p>
                                            <p><?php echo limite_resumo(320)."..."; ?></p>
                                            <p class="text-right">
                                                <a href="<?php the_permalink(); ?>" class="btn btn-primary btn-sm">Leia mais &rarr;</a>
                                            </p>
                                        </div>

                                    </div>
                                </div>
                            </div>

                         <?php 
                    } //.while  ?>
                    <hr> <?php 

                    if(function_exists("paginacao")) paginacao($agenda->max_num_pages);

                } else { ?>
                    <div class="alert alert-warning fade in">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        Nenhuma <b>item</b> encontrada nesta categoria.
                    </div> <?php 
                } // else ?>
                
                </div> <!-- .col-md-9 -->
                
                <aside class="col-md-3">

                    <?php if ( !dynamic_sidebar( 'barra-lateral-widget' )) {  } ?>
                    
                    <hr>

                    
                </aside>
            </div>
        </div>
        <!-- /container -->



<?php get_footer(); ?>