<?php

get_header();

if ( isset( $_POST['enviar-sms'] ) ) {

    if ( isset($_POST['form_name'])  &&  isset($_POST['form_email'])  &&  isset($_POST['form_comment']) ) {
        if (empty($_POST['form_name']) || empty($_POST['form_email']) || empty($_POST['form_comment']) ) {
            $_SESSION['info'] = 'Preencha todos campos corretamente.';
        } else {
            $cabecalho = 'From: '.$_POST['form_email']."\r\n".'Reply-To: '.$_POST['form_email']."\r\n".'X-Mailer: PHP/'.phpversion();
            $retorno = mail( get_bloginfo('admin_email'), "Mensagem de contato pelo site por : ".$_POST['form_name'], $_POST['form_comment'], $cabecalho );
            $_SESSION['info'] =  "Sua mensagem não pode ser enviada. Tente novamente.";
            
            if ( $retorno ) {
                $_SESSION['info'] =  "Mensagem enviada com sucesso.";
            }
        }
    }
}

?>

<div class="container">

    <ul class="breadcrumb">
        <li><a href="<?php echo get_option( "home" ); ?>">Home</a></li>
        <li class="active">Contato</li>
    </ul>

    <div class="row">
        <div class="col-xs-12">
            <h2>Contate-nos</h2>
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h4>Informação</h4>
            <p>Faça-nos uma visita!</p>
            <br />
            <h4>Centro</h4>
            <address>
                <p>Rua Dom Pedro II, 637,<br /> Centro Empresarial Porto Velho, Sala 512, 76.801-910<br />Porto Velho, RO</p>
            </address>
            <address>
                <p><i class="icon-headphones"></i>&nbsp;(069)&nbsp; 3223-9087</p>
                <p><i class="icon-envelope"></i>&nbsp;advocacia@falcaoeveloso.adv.br</p>
                <p><i class="icon-globe"></i>&nbsp;www.falcaoeveloso.adv.br</p>
            </address>
        </div>
        <div class="col-md-8">
            <h4>Nossa Localização</h4>
            <div style="padding: 5px; border: 1px solid #CCC">

<script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script>
<div style='overflow:hidden;height:300px;'>
    <div id='gmap_canvas' style='height:300px;'></div>
    <style>#gmap_canvas img{max-width:none!important;background:none!important}</style>
</div>
<script type='text/javascript'>
function init_map(){var myOptions = {zoom:18,center:new google.maps.LatLng(-8.7626797,-63.904415400000005),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(-8.7626797,-63.904415400000005)});infowindow = new google.maps.InfoWindow({content:'<strong>Falcão & Veloso Advogados Associados</strong><br>587, R. Dom Pedro II, 433 - São Cristóvão, Porto Velho - RO, 76804-091<br>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);
</script>



            </div>
            <br />
            <h4>Deixe sua mensagem</h4><?php
                if ( isset($_SESSION['info']) ) {
                    echo '<div class="info label label-warning">' . $_SESSION['info'] . '</div>';
                    unset( $_SESSION['info'] );
                } ?>
            <form action="" method="post"> 
                <label for="form_name">Nome</label>
                <input id="form_name" name="form_name" class="form-control" type="text" value="">
                <label for="form_email">Email</label>
                <input id="form_email" name="form_email" class="form-control" type="email"  value="">
                <label for="comment">Mensagem</label>
                <textarea id="form_comment" name="form_comment" class="form-control" cols="15" rows="3" value=""></textarea>
                <br>
                <button class="btn btn-primary" type="submit" name="enviar-sms">Enviar</button>
            </form>
        </div>
    </div>        
</div><!-- /container -->

<br>

<?php get_footer(); ?>