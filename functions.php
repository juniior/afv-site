<?php
/**
 * Portal dos Advogados Associados Falcão e Veloso
 * Funções e Definições
 *
 * Funciona como um tema wordpress.
 * Para auxílio em futuras implementações dentro da plataforma do wordpress, acesse http://codex.wordpress.org/.
 *
 * @package Wordpress 3.5+
 * @author Osvaldo dos Santos Junior
 * @since 0.1 (30/03/2016)
 * @version 1.0 (30/03/2016)
**/	

include('core/artigos.php');
include('core/novidades.php');
include('core/banners.php');

include('widgets/mensagem.classe.php');

/**
 * Desabilitando edição de arquivos pelo painel do Wordpress
**/
define( "DISALLOW_FILE_EDIT", true );

// Tirar a barra de admin
add_filter('show_admin_bar', '__return_false');

function remove_menu () 
{
   remove_menu_page('edit.php');
} 

add_action('admin_menu', 'remove_menu');

/**
 * Adiciona suporte a campos de upload de arquivos nas páginas de edição de posts
**/
add_action('post_edit_form_tag', 'post_edit_form_tag');
function post_edit_form_tag() {
	echo ' enctype="multipart/form-data"';
}

/**
 * Modifica a variável de paginação nos links de 'page' para 'pg'
**/
$GLOBALS["wp_rewrite"]->pagination_base = "pg";


/**
 * Configura tamanhos padrões de imagem do tema
**/
add_theme_support('post-thumbnails',array('post', 'cpt_evento', 'cpt_banner', 'cpt_destaque', 'cpt_galeria', 'cpt_musicos','comment-list', 'comment-form'));

function remove_image_sizes($sizes) {
	unset( $sizes["medium"] );
	unset( $sizes["large"] );
	return $sizes;
}

add_filter( "intermediate_image_sizes_advanced", "remove_image_sizes");
//add_image_size( $name, $width, $height, $crop );
set_post_thumbnail_size(150, 9999, false);
add_image_size("290wide", 290, 9999, false);
add_image_size("200-300", 200, 300, false);
add_image_size("img-360-296", 360, 296, true);
add_image_size("610wide", 610, 9999, false);
add_image_size("900-350", 900, 350, true);
add_image_size("263-263", 263, 263, true);
add_image_size("1170-banners", 1170, 450, false);
add_image_size("wide-620h", 9999, 620, false);
add_image_size("wide-350h", 9999, 350, false);
function add_custom_media_sizes() {
	$sizes = array( "290wide" => __("Imagem Média"), "610wide" => __("Imagem Grande"), "200-300" => __("Imagem Pequena"),"img-360-296"=>"Imagem de destaque" );
	return $sizes;
}
add_filter("image_size_names_choose", "add_custom_media_sizes");

/**
 * Função que gera busca no site
**/
function busca_sidebar() { ?>
	<article>
	    <form action="<?php echo ( empty( $action ) ) ? get_option( "home" ) : ""; ?>" method="get">
			<div class="busca pull-right ">
                <input type="text" id="busca" autocomplete="off" class="" placeholder="Busca" required="required" name="s">
            </div>
	    </form>
	</article> <?php
}

/**
 * Função que gera paginação com a estrutura e classes do Bootstrap
**/
function paginacao( $pages = "", $range = 4 ){
	$showitems = ( $range * 2 ) + 1;
	global $paged;

	if ( empty( $paged ) ) {
		$paged = 1;
	}
	if ( $pages == "" ) {
		global $wp_query;
		$pages = $wp_query->max_num_pages;
		if ( !$pages ) {
			$pages = 1;
		}
	}	
	if ( 1 != $pages ) {
		echo "<div class='text-center'><ul class='pagination'>";
		if ( $paged > 2 && $paged > $range + 1 && $showitems < $pages ) {
			echo "<li><a href='".get_pagenum_link( 1 )."'>&laquo; Primeira</a></li>";
		}
		if ( $paged > 1 && $showitems < $pages ) {
			echo "<li><a href='".get_pagenum_link( $paged - 1 )."'>&lsaquo;</a></li>";
		}
		for ( $i=1; $i <= $pages; $i++ ) {
			if ( 1 != $pages && ( !( $i >= $paged + $range + 1 || $i <= $paged - $range - 1 ) || $pages <= $showitems ) ) {
				echo ( $paged == $i )? "<li class='active'><a href='#'>$i</a></li>":"<li><a href='".get_pagenum_link( $i )."'>$i</a></li>";
			}
		}
		if ( $paged < $pages && $showitems < $pages ) {
			echo "<li><a href='".get_pagenum_link( $paged + 1 )."'>&rsaquo;</a></li>";
		}
		if ( $paged < $pages - 1 &&  $paged + $range - 1 < $pages && $showitems < $pages ) {
			echo "<li><a href='".get_pagenum_link( $pages )."'>Última &raquo;</a></li>";
		}
		echo "</ul></div>\n";
	}
}

function mes_extenso($referencia = NULL){
	switch ($referencia){
	case 1: $mes = " de Janeiro de "; break;
	case 2: $mes = " de Fevereiro de "; break;
	case 3: $mes = " de Março de "; break;
	case 4: $mes = " de Abril de "; break;
	case 5: $mes = " de Maio de "; break;
	case 6: $mes = " de Junho de "; break;
	case 7: $mes = " de Julho de "; break;
	case 8: $mes = " de Agosto de "; break;
	case 9: $mes = " de Setembro de "; break;
	case 10: $mes = " de Outubro de "; break;
	case 11: $mes = " de Novembro de "; break;
	case 12: $mes = " de Dezembro de "; break;
	default: $mes = " de _______________ de ";
	}
	return $mes;
}

function verifica_icone($mime_type) {
	$names = array("doc", "xls", "ppt", "cdr", "mp3", "txt", "xml", "jpg", "png", "gif", "wps", "otp", "odt", "ods", "odf", "zip", "rar", "pdf", "dwg", "wav", "tgz", "rtf", "psd", "iso", "tiff");
	$types = array(
		"doc" => array("application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/msword", "application/doc", "appl/text", "application/vnd.msword", "application/vnd.ms-word", "application/winword", "application/word", "application/x-msw6", "application/x-msword"),
		"xls" => array("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.ms-excel", "application/msexcel", "application/x-msexcel", "application/x-ms-excel", "application/vnd.ms-excel", "application/x-excel", "application/x-dos_ms_excel", "application/xls"),
		"ppt" => array("application/vnd.ms-powerpoint", "application/mspowerpoint", "application/ms-powerpoint", "application/mspowerpnt", "application/vnd-mspowerpoint", "application/powerpoint", "application/x-powerpoint", "application/x-m"),
		"cdr" => array("application/cdr", "application/coreldraw", "application/x-cdr", "application/x-coreldraw", "image/cdr", "image/x-cdr", "zz-application/zz-winassoc-cdr"),
		"mp3" => array("audio/mpeg", "audio/x-mpeg", "audio/mp3", "audio/x-mp3", "audio/mpeg3", "audio/x-mpeg3", "audio/mpg", "audio/x-mpg", "audio/x-mpegaudio"),
		"txt" => array("text/plain", "application/txt", "browser/internal", "text/anytext", "widetext/plain", "widetext/paragraph"),
		"xml" => array("text/xml", "application/xml", "application/x-xml"),
		"jpg" => array("image/jpeg", "image/jpg", "image/jp_", "application/jpg", "application/x-jpg", "image/pjpeg", "image/pipeg", "image/vnd.swiftview-jpeg", "image/x-xbitmap"),
		"png" => array("image/png", "application/png", "application/x-png"),
		"gif" => array("image/gif", "image/x-xbitmap", "image/gi_"),
		"eps" => array("application/postscript", "application/eps", "application/x-eps", "image/eps", "image/x-eps"),
		"otp" => array("application/vnd.oasis.opendocument.presentation", "application/x-vnd.oasis.opendocument.presentation"),
		"odt" => array("application/vnd.oasis.opendocument.text", "application/x-vnd.oasis.opendocument.text"),
		"ods" => array("application/vnd.oasis.opendocument.spreadsheet", "application/x-vnd.oasis.opendocument.spreadsheet"),
		"odf" => array("application/vnd.oasis.opendocument.formula", "application/x-vnd.oasis.opendocument.formula"),
		"zip" => array("application/zip", "application/x-zip", "application/x-zip-compressed", "application/octet-stream", "application/x-compress", "application/x-compressed", "multipart/x-zip"),
		"rar" => array("application/rar", "application/x-compressed", "application/x-rar", "application/x-rar-compressed", "application/x-rar-compressed", "application/x-compressed", "compressed/rar", "application/x-rar-compressed"),
		"pdf" => array("application/pdf", "application/x-pdf", "application/acrobat", "applications/vnd.pdf", "text/pdf", "text/x-pdf"),
		"dwg" => array("application/acad", "application/x-acad", "application/autocad_dwg", "image/x-dwg", "application/dwg", "application/x-dwg", "application/x-autocad", "image/vnd.dwg", "drawing/dwg"),
		"wav" => array("audio/wav", "audio/x-wav", "audio/wave", "audio/x-pn-wav"),
		"tgz" => array("application/x-compressed", "application/x-tar"),
		"rtf" => array("application/rtf", "application/x-rtf", "text/rtf", "text/richtext", "application/msword", "application/doc", "application/x-soffice"),
		"psd" => array("image/photoshop", "image/x-photoshop", "image/psd", "application/photoshop", "application/psd", "zz-application/zz-winassoc-psd"),
		"iso" => array("application/x-isoview"),
		"tiff" => array("image/tiff")
		);
	$cont = 0;
	foreach ($types as $type) {
		if (in_array($mime_type, $type)) {
			return "<img src=\"".cdn_resource("icones/").$names[$cont].".png\" alt=\"Icone de Arquivo ".$names[$cont]."\">";
		}
		$cont++;
	}
}

function tamanhoArquivo($size, $sep = ' ') {
	$unit = null;
	$units = array('B', 'KB', 'MB', 'GB', 'TB');
	for( $i = 0, $c = count($units); $i < $c; $i++ ) {
		if ( $size > 1024 ) {
			$size = $size / 1024;
		} else {
			$unit = $units[$i];
			break;
		}
	}
	return round( $size, 2 ).$sep.$unit;
}

/**
 * Limita resumos do wordpress a quantidade de palavras especificadas
**/
function resumo( $string, $word_limit ) {
	$words = explode(" ", $string, ($word_limit + 1));
	if ( count( $words ) > $word_limit )
		array_pop( $words );
	return implode( " ", $words );
}

// Limita os caracteres do resumo nativo do wordpress
function limite_resumo($charlength) {
	$excerpt = get_the_excerpt();
	$charlength++;
	if (mb_strlen($excerpt) > $charlength) {
		$subex = mb_substr($excerpt, 0, $charlength - 5);
		$exwords = explode(' ', $subex);
		$excut = - (mb_strlen($exwords[ count( $exwords ) - 1 ]));
		if ( $excut < 0 ) {
			$excerpt = mb_substr($subex, 0, $excut);
		} else {
			$excerpt = $subex;
		}
	}
	return $excerpt;
}



/**
 * Cria páginas específicas do tema caso não existam com base no 'Nome'
**/
function verifica_pagina( $_slug, $_nome ) {
	if ( !get_page_by_title( $_nome ) ) {
		wp_insert_post( array( "post_type" => "page", "post_author" => 1, "post_title" => $_nome, "post_name" => $_slug, "post_content" => "...", "coment_status" => "closed", "post_status" => "publish" ) );
	}
}

verifica_pagina( "artigo", "Artigos" );
verifica_pagina( "novidade", "Novidades" );
verifica_pagina( "contato", "Contato" );
verifica_pagina( "sitemap", "Sitemap" );

/**
 * Removendo as tags dos posts padrões
**/
function remove_tags_from_posts() {
	remove_meta_box( "tagsdiv-post_tag", "post", "side" );
}
add_action( 'admin_menu' , 'remove_tags_from_posts' );

/**
 * Filtro de buscas
**/
add_action( "pre_get_posts", "customize_search_display" );
function customize_search_display( $query ){
	if ( ($query->is_main_query() ) && ( is_tax() )) {
		$query->set("posts_per_page", 15);
	}
}

/******************************************************************************/
/*Exibindo as miniaturas na listagem dos posts do painel administrativo */
/******************************************************************************/

add_filter('manage_posts_columns', 'posts_columns', 5);
add_action('manage_posts_custom_column', 'posts_custom_columns', 5, 2);
function posts_columns($defaults){
	$defaults['my_post_thumbs'] = __('Imagem destacada');
	return $defaults;
}
function posts_custom_columns($column_name, $id){
	if($column_name === 'my_post_thumbs'){
		echo the_post_thumbnail( 'thumbnail' );
	}
}

/**
 * Adiciona tipos de posts personalizados as estatísticas da dashboard.
**/
add_action( "right_now_content_table_end", "dashboard_cpt" );
function dashboard_cpt() {
	echo "<tr><td colspan=\"2\" style=\"border-bottom: 1px solid #e8e8e8\"></td></tr>";
	$post_types = get_post_types( array( "_builtin" => false ), "objects" );
	foreach( $post_types as $post_type ) {
		$num_posts = wp_count_posts( $post_type->name );
		$num = number_format_i18n( $num_posts->publish ); ?>
		<tr>
			<td class="first b b_<?php echo $post_type->name; ?>">
				<a href="edit.php?post_type=<?php echo $post_type->name; ?>"><?php echo $num; ?></a>
			</td>
			<td class="t <?php echo $post_type->name; ?>">
				<a href="edit.php?post_type=<?php echo $post_type->name; ?>"><?php echo $post_type->label; ?></a>
			</td>
		</tr> <?php
	}
}

/*
add_action( "admin_head", "custom_script" );
function custom_script() { ?>
	<script type="text/javascript">
		!function ($) {
			$(function() {
				alert("Só Jesus salva.")
			});
		}(window.jQuery)
	</script> <?php
}
*/

add_action( 'show_user_profile', 'my_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'my_show_extra_profile_fields' );

// Campos do perfil personalizado
function my_show_extra_profile_fields( $user ) { ?>
	<h3>Você nas redes sociais</h3>
	<table class="form-table">
		<tr>
			<th><label for="googleuser">Google+</label></th>
			<td>
				<input type="text" name="googleuser" id="googleuser" value="<?php echo esc_attr( get_the_author_meta( 'googleuser', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">O seu perfil no Google+ (URL)</span>
			</td>
		</tr>
		<tr>
			<th><label for="twitter">Twitter</label></th>
			<td>
				<input type="text" name="twitteruser" id="twitteruser" value="<?php echo esc_attr( get_the_author_meta( 'twitteruser', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">O seu nome de usuário do Twitter</span>
			</td>
		</tr>
		<tr>
			<th><label for="facebookuser">Facebook</label></th>
			<td>
				<input type="text" name="facebookuser" id="facebookuser" value="<?php echo esc_attr( get_the_author_meta( 'facebookuser', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">O seu perfil no Facebook (URL)</span>
			</td>
		</tr>
	</table><?php 
} 
// Guardar e manter as infos nos campos
add_action( 'personal_options_update', 'my_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'my_save_extra_profile_fields' );

function my_save_extra_profile_fields( $user_id ) {
	if ( !current_user_can( 'edit_user', $user_id ) ){
		return false;
	}
	update_usermeta( $user_id, 'googleuser', $_POST['googleuser'] );
	update_usermeta( $user_id, 'twitteruser', $_POST['twitteruser'] );
	update_usermeta( $user_id, 'facebookuser', $_POST['facebookuser'] );
}

/**
 * Produz um avatar compatível com hCard
**/
function commenter_link() {
	$commenter = get_comment_author_link();
	if ( preg_match( '/<a[^>]* class=[^>]+>/', $commenter ) ) {
		$commenter = preg_replace( '/(<a[^>]* class=[\'"]?)/', '\\1url ' , $commenter );
	} else {
		$commenter = preg_replace( '/(<a )/', '\\1class="url "/' , $commenter );
	}
	$avatar_email = get_comment_author_email();
	$avatar = str_replace( "class='avatar avatar-80 photo avatar-default", "class='media-object", get_avatar( $avatar_email, 80 ) );
	echo $avatar;
} // end commenter_link


// Chamada customizada de comentários
function custom_comments($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment;
	$GLOBALS['comment_depth'] = $depth;	?>
	<li id="comment-<?php comment_ID() ?>" class="media">	
	<a  class="thumbnail pull-left" href=""><?php commenter_link() ?></a>

	<div class="media-body"> <?php 
		printf(	__('<small class="pull-right">%3$s, %1$s at %2$s </small>', 'seu-template'),	get_comment_date(),	get_comment_time(), get_comment_author_link() ); ?>
	</div> <?php 
	if ($comment->comment_approved == '0') _e("\t\t\t\t\t<mark>Seu comentário está aguardando à moderação.</mark>\n", 'seu-template') ?>
	
	<div class="comment-content">
		<?php comment_text() ?>
	</div> <?php

	if($args['type'] == 'all' || get_comment_type() == 'comment') {
		comment_reply_link( 
			array_merge($args, array(
					'reply_text' => __('Responder','seu-template'),
					'login_text' => __('Entrar para responder.','seu-template'),
					'depth' => $depth,
					'before' => '<div class="btn btn-default btn-sm">',
					'after' => '</div>'
				)
			)
		);
	}

	edit_comment_link(__('Editar', 'seu-template'), '  <span class="btn btn-default btn-sm">', '</span>'); 
} 

// Chamada customizada para listar trackbacks
function custom_pings($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment; ?>
	<li id="jr-comment-<?php comment_ID() ?>" <?php comment_class() ?>>
	<div class="comment-author"> <?php printf( __('By %1$s on %2$s at %3$s', 'seu-template'),
		get_comment_author_link(),
		get_comment_date(),
		get_comment_time() );
		edit_comment_link(__('Edit', 'seu-template'), ' <span class="meta-sep">|</span> <span class="edit-link">', '</span>'); ?>
	</div> <?php 
	if ($comment->comment_approved == '0') _e('\t\t\t\t\t<span class="unapproved">Your trackback is awaiting moderation.</span>\n', 'seu-template') ?>
	<div class="comment-content">
		<?php comment_text() ?>
	</div> <?php 
}

/**
 * Renomeando Posts para Noitícias
**/
function change_post_menu_label() {
	global $menu;
	global $submenu;
	$menu[5][0] = "Notícias";
	$submenu["edit.php"][5][0] = "Notícias";
	$submenu["edit.php"][10][0] = "Adicionar Notícia";
	$submenu["edit.php"][16][0] = "Tags";
}

function change_post_object_label() {
	global $wp_post_types;
	$labels = &$wp_post_types["post"]->labels;
	$labels->name = "Notícias";
	$labels->singular_name = "Notícias";
	$labels->add_new = "Adicionar Notícia";
	$labels->add_new_item = "Adicionar Novo Notícia";
	$labels->edit_item = "Editar Notícia";
	$labels->new_item = "Novo Notícia";
	$labels->view_item = "Ver Notícias";
	$labels->search_items = "Buscar Notícias";
	$labels->not_found = "Nenhum Notícia Encontrado";
	$labels->not_found_in_trash = "Nenhum Notícia Encontrado na Lixeira";
}
add_action( "init", "change_post_object_label" );
add_action( "admin_menu", "change_post_menu_label" );


// Remove todos os widgets padrões do Wordpress
 function unregister_default_widgets() {
     //unregister_widget('WP_Widget_Pages');
     unregister_widget('WP_Widget_Calendar');
     unregister_widget('WP_Widget_Archives');
     unregister_widget('WP_Widget_Links');
     unregister_widget('WP_Widget_Meta');
     unregister_widget('WP_Widget_Search');
     unregister_widget('WP_Widget_Text');
     unregister_widget('WP_Widget_Categories');
     //unregister_widget('WP_Widget_Recent_Posts');
     //unregister_widget('WP_Widget_Recent_Comments');
     unregister_widget('WP_Widget_RSS');
     unregister_widget('WP_Widget_Tag_Cloud');
     unregister_widget('WP_Nav_Menu_Widget');
     unregister_widget('Twenty_Eleven_Ephemera_Widget');
 }
 add_action('widgets_init', 'unregister_default_widgets', 11);


// Cria areas de WidGets para o tema
function barras_widgets_init() {
	
	register_sidebar( array(
		'name' => __( 'Área de Widget - Barra Lateral', 'barra-lateral' ),
		'id' => 'barra-lateral-widget',
		'description' => __( 'Área de Widget - Barra Lateral', 'barra-lateral' ),
		'before_widget' => '<article> <div class="panel panel-default">',
		'after_widget' => '</div> </article>',
		'before_title' => ' <div class="panel-heading">',
		'after_title' => '</div>',
	) );

	// register_sidebar( array(
	// 	'name' => __( 'Área de Widget - Coluna 1 Rodape', 'rodape-coluna-1' ),
	// 	'id' => 'rodape-coluna-1-widget',
	// 	'description' => __( 'Área de Widget - Coluna 1 Rodape', 'rodape-coluna-1' ),
	// 	'before_widget' => '<article> <div class="panel panel-default">',
	// 	'after_widget' => '</div> </article>',
	// 	'before_title' => ' <div class="panel-heading">',
	// 	'after_title' => '</div>',
	// ) );

	// register_sidebar( array(
	// 	'name' => __( 'Área de Widget - Coluna 2 Rodape', 'rodape-coluna-2' ),
	// 	'id' => 'rodape-coluna-2-widget',
	// 	'description' => __( 'Área de Widget - Coluna 2 Rodape', 'rodape-coluna-2' ),
	// 	'before_widget' => '<article> <div class="panel panel-default">',
	// 	'after_widget' => '</div> </article>',
	// 	'before_title' => ' <div class="panel-heading">',
	// 	'after_title' => '</div>',
	// ) );

	// register_sidebar( array(
	// 	'name' => __( 'Área de Widget - Coluna 3 Rodape', 'rodape-coluna-3' ),
	// 	'id' => 'rodape-coluna-3-widget',
	// 	'description' => __( 'Área de Widget - Coluna 3 Rodape', 'rodape-coluna-3' ),
	// 	'before_widget' => '<article> <div class="panel panel-default">',
	// 	'after_widget' => '</div> </article>',
	// 	'before_title' => ' <div class="panel-heading">',
	// 	'after_title' => '</div>',
	// ) );
}

add_action( 'widgets_init', 'barras_widgets_init' );

/**
 * Registra menus a serem utilizados no tema
**/
register_nav_menu("global", "Menu de navegação global");

/**
 * Walker personalizado dos menus para exibição no formato do Bootstrap
**/
class bootstrap_nav_walker extends Walker_Nav_Menu {
	
	function start_lvl( &$output, $depth = 0, $args = array()) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<ul class=\"dropdown-menu\">\n";
	}

	function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
		global $wp_query;
		$indent = ($depth) ? str_repeat("\t", $depth) : '';
		if (strcasecmp($item->title, 'divider')) {
			$class_names = $value = '';
			$classes = empty( $item->classes ) ? array() : (array) $item->classes;
			$classes[] = ($item->current) ? 'active' : '';
			$classes[] = 'menu-item-' . $item->ID;
			$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
			if ($args->has_children && $depth > 0) {
				$class_names .= ' dropdown-submenu';
			} else if($args->has_children && $depth === 0) {
				$class_names .= ' dropdown';
			}
			$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';
			$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
			$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';
			$output .= $indent . '<li' . $id . $value . $class_names .'>';
			$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
			$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
			$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
			$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
			$attributes .= ($args->has_children)        ? ' data-toggle="dropdown" data-target="#" class="dropdown-toggle"' : '';
			$item_output = $args->before;
			$item_output .= '<a'. $attributes .'>';
			$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
			$item_output .= ($args->has_children && $depth == 0) ? ' <span class="caret"></span></a>' : '</a>';
			$item_output .= $args->after;
			$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
		} else {
			$output .= $indent . '<li class="divider">';
		}
	}

	function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {
		if ( !$element ) {
			return;
		}
		
		$id_field = $this->db_fields['id'];
		if ( is_array( $args[0] ) ) {
			$args[0]['has_children'] = ! empty( $children_elements[$element->$id_field] );
		} else if (is_object($args[0])) {
			$args[0]->has_children = ! empty( $children_elements[$element->$id_field] );
		}

		$cb_args = array_merge( array(&$output, $element, $depth), $args);
		
		call_user_func_array(array(&$this, 'start_el'), $cb_args);
		
		$id = $element->$id_field;
		if (( $max_depth == 0 || $max_depth > $depth+1 ) && isset( $children_elements[$id] ) ) {
			foreach ($children_elements[ $id ] as $child) {
				if (!isset($newlevel)) {
					$newlevel = true;
					$cb_args = array_merge( array(&$output, $depth), $args);
					call_user_func_array(array(&$this, 'start_lvl'), $cb_args);
				}
				$this->display_element($child, $children_elements, $max_depth, $depth + 1, $args, $output);
			}
			unset($children_elements[$id]);
		}
		if (isset($newlevel) && $newlevel){
			$cb_args = array_merge( array(&$output, $depth), $args);
			call_user_func_array( array(&$this, 'end_lvl'), $cb_args);
		}
		$cb_args = array_merge(array(&$output, $element, $depth), $args);
		call_user_func_array(array(&$this, 'end_el'), $cb_args);
	}
}