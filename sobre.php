<div class="about well"><?php 
    $email = get_the_author_meta('email'); 
    $grav_url = "http://www.gravatar.com/avatar.php?gravatar_id=".md5($email); ?>                        
    <img alt="about" src="<?php echo $grav_url; ?>" class="img-polaroid img-rounded img-responsive" width="150px" height="150px">
    <div class="social"><?php 
        if ( get_the_author_meta( 'facebookuser' ) ) { ?>
        <a href="<?php the_author_meta( 'facebookuser' ); ?>" target="_blank"><em class="icon-facebook"></em></a><?php 
        } if ( get_the_author_meta( 'twitteruser' ) ) { ?>
            <a href="<?php the_author_meta( 'twitteruser' ); ?>" target="_blank"><em class="icon-twitter"></em></a><?php 
        } if ( get_the_author_meta( 'googleuser' ) ) { ?>
            <a href="<?php the_author_meta( 'googleuser' ); ?>?rel=autho" target="_blank"><em class="icon-google-plus"></em></a><?php     
        } ?> 
    </div>
    <h4 class="name"><?php the_author(); ?></h4>
    <p><?php echo get_the_author_meta('user_description'); ?></p>

    <div class="progress">
        <div style="width: <?php echo ((get_the_author_posts()/wp_count_posts()->publish) * 100)."%"; ?>" class="progress-bar progress-bar-success">
            PUBLICAÇÕES <?php echo ((get_the_author_posts()/wp_count_posts()->publish) * 100)."%"; ?>
        </div>
    </div>
</div>