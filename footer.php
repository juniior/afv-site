    <?php wp_footer(); ?>
        
        <footer class="rodape footer borda">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="facebook">Nossas Mídias</h3>
                </div>

                <div class="col-md-4 borda ">
                    <div class="row">
                        <div class="col-md-2">
                            <a href="https://twitter.com/falcaoeveloso" class="logo-social"><img class="" src="<?php bloginfo( "template_url" ); ?>/assets/img/ico-twitter.png" alt=""></a>
                        </div>
                        <div class="col-md-10">
<a class="twitter-timeline" href="https://twitter.com/FALCAOeVELOSO">Tweets por FALCAOeVELOSO</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
<!-- script src='<?php bloginfo( "template_url" ); ?>/assets/js/passaro.js' type='text/javascript'></script>
<script type='text/javascript'>
var twitterAccount = "falcaoeveloso";
var tweetThisText = "";
tripleflapInit();
</script> -->

                        </div>
                    </div>
                </div>

                <div class="col-md-4 borda ">
                    <div class="col-md-2">
                            <a href="#" class="logo-social"><img class="" src="<?php bloginfo( "template_url" ); ?>/assets/img/ico-facebook.png" alt=""></a>
                        </div>
                        <div class="col-md-10">
                            <div class="fb-page" data-href="https://www.facebook.com/falcaoeveloso/" data-tabs="timeline" data-width="290" data-height="150" data-small-header="true" data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/falcaoeveloso/"><a href="https://www.facebook.com/falcaoeveloso/"></a></blockquote></div></div>
                        </div>
                </div>

                <div class="col-md-4 borda item-endereco">
                    <div class="row">

                        <div class="col-md-10">
                            <h4 class=" pull-right"> <br>advocacia@falcaoeveloso.adv.br</h4> 
                        </div>
                        <div class="col-md-2">
                            <h2> <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span></h2>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-10"> <h5 class="endereco">
                                    Rua Dom Pedro II, 637 - Caiari <br>
                                    Centro Empresarial Porto Velho, Sala 512 <br>
                                    PortoVelho/RO - CEP 76.801-910
                                    </h5>
                                </div>
                                <div class="col-md-1">
                                    <h2>    <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </footer>

    </div>


    <script type="text/javascript">
        $(function () { 
            $('.ir-topo').click(function(){
                $('html,body').animate({scrollTop: 0},'slow');
            });
            // $('#myId3').jalendar({ lang: 'PT' });
        });

        $(document).ready(function(){
            var menu_item = "Home";
            $("#menu-global>li:contains('"+menu_item+"')").addClass("active");
        });

        $(function() {
            var menu = $('#menu-jr'), pos = menu.offset(), altura = $('#wpadminbar').height();
            $(window).scroll(function(){ 
                if($(this).scrollTop() > pos.top + menu.height() && menu.hasClass('padrao-jr')){
                    menu.fadeOut('fast', function(){
                        $(this).removeClass('padrao-jr').addClass('fixado-jr').fadeIn('fast');
                        if (altura > 0) {
                            $(this).addClass('aumenta-admin');
                        };
                    });
                } else if($(this).scrollTop() <= pos.top && menu.hasClass('fixado-jr')){
                    menu.fadeOut('fast', function(){
                        $(this).removeClass('fixado-jr').addClass('padrao-jr').fadeIn('fast');
                        if (altura > 0) {
                            $(this).removeClass('aumenta-admin');
                        }; 
                    });
                }
            });
        });

    </script>
</body>
<!-- facebook -->
<div id="fb-root"></div>
<script>
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

<!-- twitter -->
