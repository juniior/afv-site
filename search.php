<?php get_header(); ?>

 <div class="container"><?php 
    if( isset($_GET['s']) ) {
    	$paged = (get_query_var("paged")) ? get_query_var("paged") : 1;    	
		$_post_types = array("tipo" => array('post', 'page', 'cpt_artigo', 'cpt_novidade'));				
		$_busca = array( 'post_type' => $_post_types['tipo'], 's' => trim($_GET['s']), 'posts_per_page' => 5, 'paged' => $paged, 'orderby' => 'date', 'order' => 'desc' );
		$busca = new WP_Query($_busca); ?>
    <ul class="breadcrumb">
        <li><a href="<?php echo get_option( "home" ); ?>">Home</a></li>
        <li class="active"><?php echo $busca->found_posts; wp_title(); ?> </li>        
    </ul> 
	<div class="row"> 
	    <div class="col-md-9"><?php 
		if( $busca->have_posts()) { 
			while($busca->have_posts()) {
				$busca->the_post(); 
				$pt = get_post_type(get_the_ID()); 
				switch ($pt) {
					case 'post': $post_type = "Notícias"; break;
					case 'page': $post_type = "Páginas"; break;
					case 'cpt_artigo': $post_type = "Artigos"; break;
					case 'cpt_novidade': $post_type = "Novidades"; break;
					// case 'cpt_agenda': $post_type = "Agenda"; break;
					// case 'cpt_destaque': $post_type = "Destaques"; break;
					// case 'cpt_galeria': $post_type = "Galerias"; break;
					// case 'cpt_partituras': $post_type = "Partituras"; break;
					// case 'cpt_musicos': $post_type = "Musicos"; break;
					// case 'cpt_download': $post_type = "Downloads"; break;
					default: $post_type = "Outros"; break;
				} ?> 
		      	<div class="media"> <?php 
		            $img = get_the_post_thumbnail(get_the_ID(),'200-300',array('class'=>'pull-left img-rounded img-polaroid','title'=>'')); 
		            if( $img != '' ) { ?>
		                <a class="pull-left thumbnail" href="<?php the_permalink(); ?>"> <?php echo $img; ?> </a> <?php 
		            } ?>
		            <div class="media-body">
		                <h4 class="media-heading"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
		                <p><small>[ <?php echo $post_type; ?> ] Postado por <?php the_author_posts_link(); ?> em <?php the_time('j \d\e F \d\e Y') ?></small></p>
		                <p><?php echo limite_resumo(320)."..."; ?></p>
		                <p class="text-right">
		                    <a href="<?php the_permalink(); ?>" class="btn btn-primary btn-sm">Leia mais &rarr;</a>
		                </p>
		            </div>
		    	</div> <?php 
		    } //.while  ?>
		    <hr> <?php 

			if(function_exists("paginacao")) paginacao($busca->max_num_pages); 

		} else { ?>
		        <div class="alert alert-warning fade in">
		            <button type="button" class="close" data-dismiss="alert">×</button>
		            Nenhuma <b>item</b> encontrada nesta busca.
		        </div> <?php 
	    } // else ?>        
	    </div> <?php 
	} // .if ?>

	    <aside class="col-md-3">                    
            <?php if ( !dynamic_sidebar( 'barra-lateral-widget' )) {  } ?>
            <hr>
	    </aside>
	</div> <!-- row -->
</div>

<?php get_footer(); ?>