<?php
if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME'])){
	die ('Por favor, não carregar esta página diretamente. Thanks!');
}
if ( post_password_required() ) { ?>
  	<p class="nocomments"><?php _e('Este post é protegido por senha. Digite a senha para ver os comentários.','office'); ?></p> <?php
	return;
} 

if ( comments_open() ) { ?>
	<div id="commentsbox" class="mainBox">
		<h3>
			<?php comments_number(__('0 Comentários', ''), __('1 Comentário', ''), __('% Comentários', '') );?>
		</h3> <?php 

		if ( have_comments() ) { ?>

			<!-- inicio da listagem dos comentarios -->
			<ul class="media-list"> <?php 
				wp_list_comments( array( 'avatar_size' => 60, 'callback'	=>	'custom_comments', ) ); ?>
			</ul>
			<!-- fim da listagem dos comentarios -->

			<div class="comment-nav">
				<div class="alignleft"><?php previous_comments_link() ?></div>
				<div class="alignright"><?php next_comments_link() ?></div>
			</div><?php 
		} 	
} else {}

if ( comments_open() ) { ?>
		<hr>
		<div id="comment-form" class="well">
			<div id="respond" >
				<!-- <h3 id="comments-respond"><?php _e('Deixe uma resposta','') ?></h3> -->
				<h4>Deixe um comentário</h4>
				<div class="cancel-comment-reply">
					<?php cancel_comment_reply_link(); ?>
				</div> <?php 
			if ( get_option('comment_registration') && !is_user_logged_in() ) { ?>
				<p><?php _e('Você deve ser',''); ?> <a href="<?php echo wp_login_url( get_permalink() ); ?>"><?php _e('logado',''); ?></a><?php _e(' to post a comment.',''); ?></p> <?php 
			} else { ?>
				<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform"> <?php 
					if (is_user_logged_in()) { ?>
						<p id="comments-respond-meta">
							<?php _e('Logado como',''); ?> 
							<a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php">
								<?php echo $user_identity; ?>
							</a>. 
							<a href="<?php echo wp_logout_url(get_permalink()); ?>" title="<?php _e('Sair da conta',''); ?>">
								<?php _e('Sair',''); ?> &raquo;
							</a>
						</p> <?php 
					} else 	{ ?>
						<label for="author">Nome</label>
						<input class="form-control" type="text" name="author" id="author" value="<?php if ($comment_author == '') { echo _e('Username', '' ); echo '*'; } elseif ($comment_author >= '') { echo $comment_author; } ?>" onfocus="if(this.value=='<?php _e('Username', '' ); ?>*')this.value='';" onblur="if(this.value=='')this.value='<?php _e('Username', '' ); ?>*';" size="22" tabindex="1" />
						<label for="email">Email</label>
						<input class="form-control" type="email" name="email" id="email" value="<?php if ($comment_author_email == '') { echo _e('Email', '' ); echo '*'; } elseif ($comment_author_email >= '') { echo $comment_author_email; } ?>" onfocus="if(this.value=='<?php _e('Email', '' ); ?>*')this.value='';" onblur="if(this.value=='')this.value='<?php _e('Email', '' ); ?>*';" size="2" tabindex="2" />
						<label for="url">Site</label>
						<input class="form-control" type="text" name="url" id="url" value="<?php if ($comment_author_url == '') { echo _e('Website', '' ); } elseif ($comment_author_url >= '') { echo $comment_author_url; } ?>" onfocus="if(this.value=='<?php _e('Website', '' ); ?>')this.value='';" onblur="if(this.value=='')this.value='<?php _e('Website', '' ); ?>';" size="2" tabindex="3" /> <?php 
					} ?>
					<label for="comment">Mensagem</label>
					<textarea class="form-control" name="comment" id="comment" rows="10" cols="15" rows="3" tabindex="4"></textarea>
					<br>
					<button type="submit" id="commentSubmit" class="button buttonShadow btn btn-default"><span><?php _e('Comentar Sobre', '' ); ?></span></button> <?php 
					comment_id_fields(); 
				 	do_action('comment_form', $post->ID); ?>
				</form> <?php 
			} ?>
			</div>
		</div>
	</div> <?php 	
} else {}
?>