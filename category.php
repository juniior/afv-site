<?php get_header(); ?>

<div class="container"> <?php 
    $_noticias = array('orderby' => 'date', 'order' => 'desc', 'posts_per_page' => 20, 'paged' => ((get_query_var('paged')) ? get_query_var('paged') : 1), 'cat' => get_query_var('cat') );   
    $noticias = new WP_Query($_noticias); ?>

    <ul class="breadcrumb">
        <li> <a href="<?php echo get_option( "home" ); ?>">Home</a> </li>
        <li class="active"><?php wp_title(); ?></li>
    </ul>

    <div class="row">
        <div class="col-md-9"> <?php  
            if( $noticias->have_posts()) { 
                while($noticias->have_posts()) {
                    $noticias->the_post(); ?>
                    <div class="media"> <?php 
                        $img = get_the_post_thumbnail(get_the_ID(),'200-300', array('class'=>'pull-left img-rounded img-polaroid','title'=>'')); 
                        if( $img != '' ) { ?>
                            <a class="pull-left thumbnail" href="<?php the_permalink(); ?>"> <?php echo $img; ?> </a> <?php 
                        } ?>
                        <div class="media-body">
                            <h4 class="media-heading"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                            <p><small>[ <?php wp_title(""); ?> ] Postado por <?php the_author_posts_link(); ?> em <?php the_time('j \d\e F \d\e Y') ?></small></p>
                            <p><?php echo limite_resumo(320)."..."; ?></p>
                            <p class="text-right">
                                <a href="<?php the_permalink(); ?>" class="btn btn-primary btn-sm">Leia mais &rarr;</a>
                            </p>
                        </div>
                    </div> <?php 
                } //.while  ?>
                <hr> <?php 

                if(function_exists("paginacao")) paginacao($noticias->max_num_pages);

            } else { ?>
                <div class="alert alert-warning fade in">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    Nenhuma <b>item</b> encontrada nesta categoria.
                </div> <?php 
            } // else ?>        
        </div> 

        <aside class="col-md-3">
            <?php if (function_exists('busca_sidebar')) { busca_sidebar(); } ?>
            <hr>
            <?php if ( !dynamic_sidebar( 'barra-lateral-widget' )) {  } ?>
            <hr>

            <article>
                <div class="panel panel-default">
                    <div class="panel-heading">Categorias</div> <?php 
                        $args = array( 'type' => 'post', 'orderby' => 'name', 'order' => 'ASC', 'hide_empty' => 1, 'hierarchical' => 1, 'taxonomy' => 'category' ); 
                        $categorias = get_categories($args); ?>
                        <div class="list-group"> <?php 
                        foreach($categorias as $categoria){ ?>
                            <a class="list-group-item" href="<?php echo get_category_link($categoria->term_id); ?>"><em class="icon-sort-by-attributes"></em> &nbsp; <?php echo $categoria->name; ?></a> <?php 
                        } ?>
                    </div>
                </div>
            </article>
        </aside>

    </div> <!-- .row -->

</div>

<?php get_footer(); ?>