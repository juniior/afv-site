<?php get_header(); ?>

        <div class="container">

            <ul class="breadcrumb">
                <li> <a href="<?php echo get_option( "home" ); ?>">Home</a> </li>
                <li class="active"><?php the_title(); ?> </li>
            </ul>

            <div class="row">
                <div class="col-xs-12">
                    <h2><?php the_title(); ?></h2>
                    <hr>
                </div>
            </div>
            <div class="row">
            <?php if(have_posts()): while(have_posts()): the_post(); ?>
                <div class="col-md-9"><?php
                    if ( has_post_thumbnail() ) { ?>
                        <div class="thumbnail">
                            <?php the_post_thumbnail('900-350', array('class' => 'img-polaroid img-rounded img-responsive',"alt" => get_the_title())); ?>
                        </div> <br> <?php
                    } ?>
                    
                    <p><small>Postado por <?php the_author_posts_link(); ?> em <?php the_time('j \d\e F \d\e Y') ?></small></p>
                    
                    <?php the_content(); ?>
                    
                    <hr>
                    <br>
                    <br>

                    <?php get_template_part('galeria','') ?>

                    <hr>
                    <br>
                    <br>
                    
                    <?php comments_template(); ?>
                    
                </div>
                <!-- post list -->
            <?php endwhile; endif; ?>

                <aside class="col-md-3">

                    <?php if ( !dynamic_sidebar( 'barra-lateral-widget' )) {  } ?>
                    <hr>
                                         
                </aside>
            </div>
        </div>

<?php get_footer(); ?>