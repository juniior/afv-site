<?php get_header();?>

    <div class="row corpo"> <?php 
        $banners = new WP_Query( array( "post_type" => "cpt_banner", "posts_per_page" => 4, "orderby" => "date", "order" => "DESC" ) );
        if( $banners->have_posts() ) 
        { ?>
            <div class="container-full borda">
                <div id="myCarousel" class="carousel slide banners">
                    <ol class="carousel-indicators"><?php  $i = 0;
                    while( $banners->have_posts() ) 
                    {
                        $banners->the_post(); 
                        if ( $i == 0 )
                        { 
                            $i++; ?>
                            <li data-target="#myCarousel" data-slide-to="<?php echo get_the_ID(); ?>" class="active"></li> <?php   
                        } 
                        else 
                        { ?>
                            <li data-target="#myCarousel" data-slide-to="<?php echo get_the_ID(); ?>"></li> <?php 
                        }
                    } ?>
                    </ol>
                    <div class="carousel-inner"><?php $i = 0;
                    while( $banners->have_posts() ) 
                    {
                        $banners->the_post();
                        if ( $i == 0 )
                        { $i++; ?>
                            <div class="item active"><?php   
                        } 
                        else 
                        { ?>
                            <div class="item"><?php 
                        } ?>            
                            <a href="<?php the_permalink(); ?>">
                                <?php the_post_thumbnail('wide-350h', array('class' => 'img-polaroid img-rounded img-responsive')); ?>
                                <div class="carousel-caption caption-right visible-lg">
                                    <h2 class="carousel-caption-heading"><?php the_title(); ?></h2>
                                </div>
                            </a>
                        </div> <?php 
                    } ?>
                    </div>
                    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                        <span class="icon-prev"></span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" data-slide="next">
                        <span class="icon-next"></span>
                    </a>
                </div> <!-- .myCarousel --> 
            </div> <?php 
        } ?>

        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4">
                    <h2 class="icon-falcao">Novidades</h2><?php

                    $avisos = new WP_Query( array( "post_type" => "cpt_novidade", "posts_per_page" => 4, "orderby" => "date", "order" => "DESC" ) );

                    if( $avisos->have_posts() ) 
                    {
                        while( $avisos->have_posts() ) 
                        {
                            $avisos->the_post(); 
                            $categories = get_the_terms($avisos->ID, 'tx-novidade'); ?>

                            <div class="item">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="item-data-dia"><?php the_time('j') ?></div>
                                        <div class="item-data-mes"><?php the_time('M') ?></div>
                                    </div>
                                    <div class="col-md-10">
                                        <a href="<?php the_permalink(); ?>"><div class="item-titulo"><?php the_title(); ?></div></a>
                                        <div class="item-categoria"><?php echo $categories[0]->name; ?></div>
                                        <a href="<?php the_permalink(); ?>"> <div class="item-resumo"> <?php the_excerpt(); ?> </div> </a>
                                    </div>
                                </div>
                            </div> <?php 
                        }
                    }
                    else {
                            echo "Não há novidade cadastrados!";
                    } ?>
                </div>

                <div class="col-md-4">
                    <h2 class="icon-falcao">Artigos</h2><?php
                    $avisos = new WP_Query( array( "post_type" => "cpt_artigo", "orderby" => "date", "order" => "DESC", "posts_per_page" => 5 ) );
                    if( $avisos->have_posts() ) 
                    { 
                        while( $avisos->have_posts() ) 
                        {
                            $avisos->the_post(); ?>
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="item-data-dia"><?php the_time('j') ?></div>
                                        <div class="item-data-mes"><?php the_time('M') ?></div>
                                    </div>
                                    <div class="col-md-10">
                                        <a href="<?php the_permalink(); ?>"><div class="item-titulo"><?php the_title(); ?></div></a>
                                        <div class="item-categoria"><?php the_author_posts_link(); ?></div>
                                        <a href="<?php the_permalink(); ?>"> <div class="item-resumo"> <?php the_excerpt(); ?> </div> </a>
                                    </div>
                                </div>
                            </div> <?php 
                        }
                    } 
                    else 
                    {
                        echo "Não tem artigos cadastrados!";
                    } ?>
                </div>

                <div class="col-md-4">
                    <h2 class="icon-falcao">Fale Conosco</h2>

                    <?php 
                    if ( isset( $_POST['enviar-sms'] ) ) {

                        if ( isset($_POST['form_name'])  &&  isset($_POST['form_email'])  &&  isset($_POST['form_comment']) ) {
                            if (empty($_POST['form_name']) || empty($_POST['form_email']) || empty($_POST['form_comment']) ) {
                                $_SESSION['info'] = 'Preencha todos campos corretamente.';
                            } else {
                                $cabecalho = 'From: '.$_POST['form_email']."\r\n".'Reply-To: '.$_POST['form_email']."\r\n".'X-Mailer: PHP/'.phpversion();
                                $retorno = mail( get_bloginfo('admin_email'), "Mensagem de contato pelo site por : ".$_POST['form_name'] .' Assunto: '. $_POST['form_assunto'], $_POST['form_comment'], $cabecalho );
                                $_SESSION['info'] =  "Sua mensagem não pode ser enviada. Tente novamente.";
                                
                                if ( $retorno ) {
                                    $_SESSION['info'] =  "Mensagem enviada com sucesso.";
                                }
                            }
                        }
                    } ?>
                    <div class="row"><?php
                if ( isset($_SESSION['info']) ) {
                    echo '<div class="info label label-warning">' . $_SESSION['info'] . '</div>';
                    unset( $_SESSION['info'] );
                } ?>

                        <form action="" method="post"> 
                            <div class="col-md-12">
                                <input type="text" name="form_name" class="form-control item-form" placeholder="Nome">
                            </div>
                            <div class="col-md-12">
                                <input type="email" name="form_email" class="form-control item-form" placeholder="E-mail">
                            </div>
                            <div class="col-md-12">
                                <input type="text" name="form_assunto" class="form-control item-form" placeholder="Assunto">
                            </div>
                            <div class="col-md-12">
                                <textarea cols="40" rows="5" name="form_comment" class="form-control item-form" placeholder="Descrição"> </textarea>
                            </div>
                            <div class="col-md-12">
                                <button type="button" name="enviar-sms" class="btn btn-danger pull-right btn-enviar">Enviar</button>
                            </div>

                        </form>
                    </div>
                    <hr>
                    <?php if ( !dynamic_sidebar( 'barra-lateral-widget' )) {  } ?>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>