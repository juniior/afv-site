<?php get_header(); ?>

<div class="container">
	<div class="row">
		<div class="span12">
			<div class="jumbotron large">
				<h1>Página não encontrada!</h1>
				<p class="lead"><b>Erro 404!</b> Esta página não existe.</p>
				<a class="btn btn-large btn-azul" href="<?php echo get_option( "home" ); ?>">Ir para o início do site</a>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>