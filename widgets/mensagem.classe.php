<?php 
class mensagemWidget extends WP_Widget {

	function mensagemWidget() {
		$widget_ops = array('classname' => 'mensagemWidget', 'description' => __('Mostra uma mensagem.', 'tga') );
		$this->WP_Widget('mensagemWidget', __('Mensagem','tga'), $widget_ops);
	}

	function widget($args, $instance) {
		extract($args, EXTR_SKIP);

		$titulo = empty($instance['titulo']) ? "Titulo da Mensagem" : apply_filters('widget_title',$instance['titulo']);
		$texto_w = empty($instance['texto_w']) ? "Texto da Mensagem" : apply_filters('widget_texto_w', $instance['texto_w']);

		echo '<article> <div class="panel panel-default">';
		echo '<div class="panel-heading">'. $titulo .'</div>';
		echo '<div class="panel-body"> <p>'. $texto_w .'</p> </div>';
		echo '</div> </article>';
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['titulo'] = strip_tags($new_instance['titulo']);
		$instance['texto_w'] = strip_tags($new_instance['texto_w']);		
		return $instance;
	}

	function form($instance) {
		$instance = wp_parse_args( (array)$instance, array('titulo' => '', 'texto_w' => '',));
		$titulo = strip_tags($instance['titulo']);
		$texto_w = strip_tags($instance['texto_w']); ?>
		<p>
			<label for="<?php echo $this->get_field_id('titulo');?>"><?php _e('Título:', 'tga') ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('titulo'); ?>" name="<?php echo $this->get_field_name('titulo'); ?>" value="<?php echo esc_attr($titulo); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('texto_w');?>">Mensagem:</label>			
			<textarea class="widefat" rows="16" cols="20" id="<?php echo $this->get_field_id('texto_w'); ?>" name="<?php echo $this->get_field_name('texto_w'); ?>"><?php echo $texto_w; ?></textarea>			
		</p> <?php
	}

}

add_action('widgets_init', 'mensagemWidget'); 
function mensagemWidget() {
  	register_widget( 'mensagemWidget' );
}