<?php 
/**
 * Criando os tipos de posts para os Banners do site
**/
add_action('init','create_cpt_banner');
function create_cpt_banner() {
	register_post_type(
		'cpt_banner',
		array(
			'labels' => array(
				'name' => 'Banners',
				'singular_name' => 'Banner',
				'add_new' => 'Adicionar Novo',
				'all_items' => 'Todos os Banners',
				'add_new_item' => 'Adicionar Novo Banner',
				'edit' => 'Alterar',
				'edit_item' => 'Alterar Banner',
				'new_item' => 'Novo Banner',
				'view' => 'Visualizar',
				'view_item' => 'Visualizar Banner',
				'search_items' => 'Buscar Banners',
				'not_found' => 'Nenhum Banner encontrado',
				'not_found_in_trash' => 'Nenhum Banner encontrado na lixeira',
				'parent' => 'Banner Pai'
			),
		'description' => 'Banners de divulgação da entidade. Os banners de divulgação da entidade deverão ser cadastrados com Título (para fim de organização - o mesmo não aparecerá no front-end), Link e Imagem de Capa.',
		'public' => true,
		'exclude_from_search' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_nav_menus' => false,
		'show_in_menu' => true,
		'show_in_admin_bar' => true,
		'menu_position' => 5,
		'menu_icon' => null,
		'capability_type' => 'page',
		'hierarchical' => false,
		'supports' => array( 'title', 'thumbnail', 'editor' )
		)
	);
}
add_action('admin_init','ui_link_banner');
function ui_link_banner() {
	add_meta_box('cf_link_banner', 'Link', 'cf_link_banner', 'cpt_banner', 'normal', 'high');
}
function cf_link_banner($post) {
	$link = esc_html(get_post_meta($post->ID, 'link', true)); ?>
	<table style="width:100%">
		<tr><td><input type="text" name="link" style="width:100%" <?php if(!empty($link)) { echo 'value="'.$link.'"'; } ?> /></td></tr>
	</table> <?php
}
add_action('save_post','save_ui_banner',10,2);
function save_ui_banner($post_ID,$post) {
	if($post->post_type == 'cpt_banner') {
		if(isset($_POST['link'])) {
			update_post_meta($post_ID,'link',$_POST['link']);
		}
	}
}
add_filter('manage_edit-cpt_banner_columns','cols_banner');
function cols_banner($cols) {
	$cols['link'] = 'Link';
	unset($cols['comments']);
	return $cols;
}
add_action('manage_posts_custom_column','populate_cols_banner');
function populate_cols_banner($col) {
	if('link' == $col) {
		$link = esc_html(get_post_meta(get_the_ID(),'link',true));
		echo $link;
	}
}

add_filter('manage_edit-cpt_produto_sortable_columns','sort_cf_banner');
function sort_cf_banner($cols) {
	$cols['link'] = 'link';
	return $cols;
}
add_filter('request','banner_orderby');
function banner_orderby($vars) {
	if(!is_admin())
		return $vars;
	if(isset($vars['orderby']) && ('link' == $vars['orderby'])) {
		$vars = array_merge( $vars, array( 'meta_key' => 'link', 'orderby' => 'meta_value' ) );
	}
	return $vars;
}