<?php 
/**
 * Criando os tipos de posts para os Novidades
**/
add_action('init','create_cpt_novidade');
function create_cpt_novidade() {
	register_post_type(
		'cpt_novidade',
		array(
			'labels' => array(
				'name' => 'Novidades',
				'singular_name' => 'Novidade',
				'add_new' => 'Adicionar Novo',
				'all_items' => 'Todos os Novidades',
				'add_new_item' => 'Adicionar nova Novidade',
				'edit' => 'Alterar',
				'edit_item' => 'Alterar Novidade',
				'new_item' => 'Novo Novidade',
				'view' => 'Visualizar',
				'view_item' => 'Visualizar Novidade',
				'search_items' => 'Buscar Novidades',
				'not_found' => 'Nenhum Novidade encontrado',
				'not_found_in_trash' => 'Nenhum Novidade encontrado na lixeira',
				'parent' => 'Item Pai'
			),
			'public' => true,
			'exclude_from_search' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_nav_menus' => false,
			'show_in_menu' => true,
			'show_in_admin_bar' => true,
			'menu_position' => 5,
			'menu_icon' => null,
			'capability_type' => 'page',
			'hierarchical' => false,
			'supports' => array( 'title', 'editor', 'thumbnail', 'comments', 'excerpt' ),
			'rewrite' => array( 'slug'=>'novidade' ),
		)
	);
}

add_action('init','novidade_taxonomy',0);
function novidade_taxonomy() {
	register_taxonomy(
		'tx-novidade',
		'cpt_novidade',
		array(
			array(
				'name' => 'Categorias dos novidade',
				'add_new_item' => 'Adicionar nova Categoria para as novidades',
				'new_item_name' => 'Nova Categoria de novidades'
			),
			'public' => true,
			'show_ui' => true,
			'show_tagcloud' => false,
			'show_admin_column' => true,
			'hierarchical' => true,
			'label'=>'Categorias das novidades',
			'rewrite' => array( 'slug' => 'cat-novidade' )
		)
	);
}