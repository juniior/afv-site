<?php 
/**
 * Criando os tipos de posts para os artigos
**/
add_action('init','create_cpt_artigo');
function create_cpt_artigo() {
	register_post_type(
		'cpt_artigo',
		array(
			'labels' => array(
				'name' => 'Artigos',
				'singular_name' => 'Artigo',
				'add_new' => 'Adicionar Novo',
				'all_items' => 'Todos os Itens',
				'add_new_item' => 'Adicionar Novo Item',
				'edit' => 'Alterar',
				'edit_item' => 'Alterar Item',
				'new_item' => 'Novo Item',
				'view' => 'Visualizar',
				'view_item' => 'Visualizar Item',
				'search_items' => 'Buscar itens',
				'not_found' => 'Nenhum item encontrado',
				'not_found_in_trash' => 'Nenhum item encontrado na lixeira',
				'parent' => 'Item Pai'
			),
			'public' => true,
			'exclude_from_search' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_nav_menus' => false,
			'show_in_menu' => true,
			'show_in_admin_bar' => true,
			'menu_position' => 5,
			'menu_icon' => null,
			'capability_type' => 'page',
			'hierarchical' => false,
			'supports' => array( 'title', 'editor', 'thumbnail', 'comments', 'excerpt' ),
			'rewrite' => array( 'slug'=>'artigo' ),
		)
	);
}

add_action('init','artigo_taxonomy',0);
function artigo_taxonomy() {
	register_taxonomy(
		'tx-artigo',
		'cpt_artigo',
		array(
			array(
				'name' => 'Categorias dos artigo',
				'add_new_item' => 'Adicionar nova Categoria para os artigos',
				'new_item_name' => 'Nova Categoria de artigos'
			),
			'public' => true,
			'show_ui' => true,
			'show_tagcloud' => false,
			'show_admin_column' => true,
			'hierarchical' => true,
			'label'=>'Categorias dos artigos',
			'rewrite' => array( 'slug' => 'cat-artigo' )
		)
	);
}
