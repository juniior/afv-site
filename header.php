<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="site de  divulgação de trabalhos da orquestra hamornia celeste">
    <meta name="description" content="<?php if (is_single()) { single_post_title('',true); } else { bloginfo('name'); echo " - "; bloginfo('description'); } ?>">
    <meta name="author" content="Juniior Santos">
    <title><?php
        if ( function_exists( "is_tag" ) && is_tag() ) {
            single_tag_title( "Tag para &quot;" );
            echo "&quot; - ";
        } else if ( is_archive() ) {
            echo "Categorias - ";
            wp_title("");
            echo " - ";
        } elseif ( is_search() ) {
            echo "Busca por &quot;" .  $s  . "&quot; - ";
        } elseif ( !( is_404() ) && ( is_single() ) || ( is_page() ) ) {
            wp_title("");
            echo " - ";
        } elseif ( is_404() ) {
            echo "Página não encontrada - ";
        }
        if ( is_home() ) {
            bloginfo( "name" );
            echo " - ";
            bloginfo( "description" );
        } else {
            bloginfo( "name" );
        }
        if ( $paged > 1 ) {
            echo " - Página " . $paged;
        } ?>
    </title>

    <!-- <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic' rel='stylesheet' type='text/css'> -->
    <!-- <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet"> -->
    
    <link href="<?php bloginfo( "template_url" ); ?>/assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php bloginfo( "template_url" ); ?>/assets/css/main.css" rel="stylesheet">
    
    <link rel="shortcut icon" href="<?php bloginfo( "template_url" ); ?>/assets/img/favicon.ico">
        
    

    <script src="<?php bloginfo( "template_url" ); ?>/assets/js/jquery-2.2.2.min.js"></script>
    <script src="<?php bloginfo( "template_url" ); ?>/assets/js/bootstrap.min.js"></script>
    <script src="<?php bloginfo( "template_url" ); ?>/assets/js/main.js"></script>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Enable media queries on older browsers -->
    <!--[if lt IE 9]>
    <script src="<?php bloginfo( "template_url" ); ?>/assets/js/respond.min.js"></script>
    <![endif]-->

    <!-- galeria -->

    <?php wp_head(); ?>
</head>

<body>

<div class="container ">
   <header class="row topo">
            <div class="col-md-5">
                <a href="<?php echo get_option( "home" ); ?>" class="logo "><img class="" src="<?php bloginfo( "template_url" ); ?>/assets/img/logo.png" alt=""></a>
            </div>
            <div class="col-md-7">
                <div class="row">
                    <div class="col-md-12">
                        <?php if (function_exists('busca_sidebar')) { busca_sidebar(); } ?>
                    </div>
                    
                    <div class="col-md-12 menu borda">
                        <nav class="navbar" id="menu-jr">
                            <!-- <div class="container"> -->
                                <div id="navbar" class="collapse navbar-collapse">
                                    
                                    <?php wp_nav_menu( array( "theme_location" => "global", "menu_id" => "menu-global", "depth" => 4, "container" => false, "menu_class" => "nav navbar-nav onhover navbar-right oc-container", "walker" => new bootstrap_nav_walker())); ?>
                                </div>
                            <!-- </div> -->
                        </nav>

                    </div>
                </div>
            </div>
    </header>
        